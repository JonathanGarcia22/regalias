﻿    function showLoading() {
        var nav = navigator.appName;
        if (nav == "Microsoft Internet Explorer") {
            // Convertimos en minusculas la cadena que devuelve userAgent
            var ie = navigator.userAgent.toLowerCase();
            // Extraemos de la cadena la version de IE
            var version = parseInt(ie.split('msie')[1]);
            // Dependiendo de la version mostramos un resultado
            if (version < 10) {
                $('.dna').remove();

                var nuevo = document.createElement("div");
                nuevo.className = 'gif_dna';
                $('#loading').removeClass('divLoading');
                $('#loading').addClass('divLoading_gif');
                $('#loading').append(nuevo);
                $('#loading').removeClass('postLoad');
                $('#loading').addClass('preLoad');
            }
            else {
                $('#loading').removeClass('postLoad');
                $('#loading').addClass('preLoad');
            }
        }
        else {
            $('#loading').removeClass('postLoad');
            $('#loading').addClass('preLoad');
        }
    }

    function hideLoading() {
        $('#loading').removeClass('preLoad');
        $('#loading').addClass('postLoad');
    }
    //Funcion para visualizar mensajes enviados por el cliente o el servidor
    function messageSuccess(title, message) {
        var mensaje = $('<h4>' + title + '</h4>' + '<p>' + message + '</p>');
        $("#msj-success").html(mensaje);
        if (!$('#msj-success').is('.in')) {
            $('#msj-success').addClass('in');
            setTimeout(function () {
                $('#msj-success').removeClass('in');
            }, 5000);
        }
    }
    //Funcion para visualizar mensajes de error enviados por el cliente o el servidor
    function messageError(title, error) {
        var mensaje = $('<h4>' + title + '</h4>' + '<p>' + error + '</p>');
        $("#msj-error").html(mensaje);
        if (!$('#msj-error').is('.in')) {
            $('#msj-error').addClass('in');
            setTimeout(function () {
                $('#msj-error').removeClass('in');
            }, 5000);
        }
    }
