angular.module('clicSaludApp.constantes', [])

  .constant('REGALIAS', {
    url: 'data/regalias.json' 
  })
   .constant('DESARROLLO_ADMINISTRATIVO', {
    url: 'data/desarrollo_administrativo.json' 
  })
   .constant('EQUIPO', {
    url: 'data/equipo.json' 
  })
  .constant('TIME_REFRESH', {
    time: 5000
  })      
 
