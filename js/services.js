var timeout = 17000;
angular.module('clicSaludApp.services', ['clicSaludApp.constantes'])



.factory('regalias', function($http, REGALIAS) {
  var oRegalias= {
    todos : []
  };

  oRegalias.getAll = function(preload) {
      return $http({
        method: 'GET',
        url: REGALIAS.url,
        timeout:timeout
      }).success(function(data){
        oRegalias.todos = data;
        //console.log(data)
        return oRegalias.todos;
 
      }).error(function(response,status,headers,config) {
        //ErrorPeticion.mostrarError(status, preload);
      });


  }  
  return oRegalias;
})


.factory('regalias_administrativo', function($http, DESARROLLO_ADMINISTRATIVO) {
  var oRegalias= {
    todos : []
  };

  oRegalias.getAll = function(preload) {
      return $http({
        method: 'GET',
        url: DESARROLLO_ADMINISTRATIVO.url,
        timeout:timeout
      }).success(function(data){
        oRegalias.todos = data;
        //console.log(data)
        return oRegalias.todos;
 
      }).error(function(response,status,headers,config) {
        //ErrorPeticion.mostrarError(status, preload);
      });


  }  
  return oRegalias;
})

.factory('regalias_equipo', function($http, EQUIPO) {
  var oRegalias= {
    todos : []
  };

  oRegalias.getAll = function(preload) {
      return $http({
        method: 'GET',
        url: EQUIPO.url,
        timeout:timeout
      }).success(function(data){
        oRegalias.todos = data;
        //console.log(data)
        return oRegalias.todos;
 
      }).error(function(response,status,headers,config) {
        //ErrorPeticion.mostrarError(status, preload);
      });


  }  
  return oRegalias;
})


