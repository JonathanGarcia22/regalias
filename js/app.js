﻿'use strict';
angular.module('clicSaludApp', [
  'ui.router',
  'clicSaludApp.constantes', // Se inyecta las constantes
  'clicSaludApp.services', // Se inyecta los servicios
  'clicSaludApp.menu_visitante', // Se inyecta el menu
  'clicSaludApp.menu_lateral', // Se inyecta el menu lateral 
  'clicSaludApp.home', // Se inyecta la vista home
  'clicSaludApp.dashboard', // Se inyecta la vista dashboard
  'clicSaludApp.historia', // Se inyecta la vista metricas
  'clicSaludApp.principal', // Se inyecta la vista home
  'clicSaludApp.prorroga', // Se inyecta la vista prorroga
  'clicSaludApp.desarrollo_Administrativo', // Se inyecta la vista desarrollo administrativo
  'clicSaludApp.equipo' // Se inyecta la vista equipo


 
])
.config(function ($stateProvider, $urlRouterProvider, $compileProvider, $httpProvider) {

    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);

    $urlRouterProvider.otherwise('/principal/');
    $stateProvider.

        state('principal', {
            url: '/principal/:objetivo',
            templateUrl: 'templates/principal.html',
            controller: 'principalCtrl'
        })
   
});
