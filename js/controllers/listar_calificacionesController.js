'use strict';

angular.module('clicSaludApp.listado_calificaciones', ['clicSaludApp.constantes'])

.controller('CalificacionesCtrl', ['$scope', '$http', '$window', '$state', '$location', '$stateParams', 'GET_PUBLICACIONES_SIMPLE', 'GET_FILTER_PERFIL', 'scatter_eps_uno', 'scatter_eps_dos', 'scatter_eps_tres', 'scatter_ips_uno', 'scatter_ips_dos', 'scatter_ips_tres', 'scatter_semana_uno', 'scatter_semana_dos', 'scatter_semana_tres','TIME_REFRESH','$interval',
function ($scope, $http, $window, $state, $location, $stateParams, GET_PUBLICACIONES_SIMPLE, GET_FILTER_PERFIL, scatter_eps_uno, scatter_eps_dos, scatter_eps_tres, scatter_ips_uno, scatter_ips_dos, scatter_ips_tres, scatter_semana_uno, scatter_semana_dos, scatter_semana_tres,TIME_REFRESH,$interval) {
    $scope.inicializar = function () {

          $("li.contenido-deslizable > a.menu-item").on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $(".item-contenido-deslizable").removeClass("active");
            $(this).next(".item-contenido-deslizable").toggleClass("active");
          });

          $("li.contenido-deslizable > a.menu-item").on('mouseenter', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $(".item-contenido-deslizable").removeClass("active");
          });

          $( ".fa-chevron-down" ).click(function() {
            var $ele = $(this).parents('.panel-heading');
              $ele.parents( ".grid-stack-item-content" ).toggleClass( "por-collapse", 500 );
            });

          $(".fa-close").on("click", function() {
            var $ele = $(this).parents('.grid-stack-item-content');
            $ele.addClass('panel-close');
            setTimeout(function() {
              $ele.parent().remove();
            }, 0);
          });

          $(".fa-rotate-right").on("click", function() {
            var $ele = $(this).parents('.panel-heading').siblings('.esconder-caja');
            $ele.append('<div class="overlay"><div class="overlay-content"><i class="fa fa-refresh fa-2x fa-spin"></i></div></div>');
            setTimeout(function() {
              $ele.find('.overlay').remove();
            }, 2000);
          });

          /* ====== GRILLA GRIDSTACK ======== */

           $('.grid-stack').gridstack({
                 width: 12,
                 cellHeight: 62,
                 verticalMargin: 10,
                 animate :true
             });

             /* == TERMINA GRILLA GRIDSTACK == */

          };

          /* == FUNCION EFECTO FLIP EN CADA CAJA == */

          $scope.flip=function(caja) {
                   $("."+caja).toggleClass('flipped');
          };

          /* == TERMINA FUNCION EFECTO FLIP EN CADA CAJA == */


          /* == FUNCION CALENDARIO == */


          $(function() {

              function cb(start, end) {
                  $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
              }
              cb(moment().subtract(29, 'days'), moment());

              $('#reportrange').daterangepicker({
                "locale": {
                  "format": "MM/DD/YYYY",
                  "separator": " - ",
                  "applyLabel": "Aplicar",
                  "cancelLabel": "Cancelar",
                  "fromLabel": "From",
                  "toLabel": "To",
                  "customRangeLabel": "Elegir Fechas",
                  "daysOfWeek": [
                      "Do",
                      "Lu",
                      "Ma",
                      "Mi",
                      "Ju",
                      "Vi",
                      "Sa"
                  ],
                  "monthNames": [
                      "Enero",
                      "Febrero",
                      "Marzo",
                      "Abril",
                      "Mayo",
                      "Junio",
                      "Julio",
                      "Agusto",
                      "Septiembre",
                      "Octubre",
                      "Noviembre",
                      "Diciembre"
                  ],
                  "firstDay": 1
              },
              "opens": "left",
                  ranges: {
                     'Hoy': [moment(), moment()],
                     'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                     'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
                     'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
                     'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                     'El Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                  }
              }, cb);

          });

        /* == TERMINA FUNCION CALENDARIO == */




        /* == GRAFICAS PAGINA LISTAR CALIFICACIONES == */

        /*
          var oJSON=[{nombre: 'sura', valor:[1,2,4,5,6]},{nombre: 'comfaboy', valor:[1,2,3,4,5]}];
          console.log(oJSON)
          for(var i in oJSON  ) {
          }  
        */

        /* ===== SECCION GRAFICA SCATTER EPS UNO ===== */  
      function fn_Scatter_Eps_uno(){ 
        scatter_eps_uno.getAll().then(function(){

        var valoresScatterEpsUno=scatter_eps_uno.todos;
        var scatter_uno = c3.generate({ 
            bindto: '#scatter_uno',
              padding: {
                  right: 50,
                  left: 50
              },
              size: {
                    height: 300
              },
              color: {
                    pattern: ['#0060B5', '#AF0037']
              },
              data: {
              xs: {
                  Real: 'Real_x',
                  Estimado: 'Estimado_x'
              },
                  // iris data from R
                  columns:valoresScatterEpsUno,
                  type: 'scatter',
                  types: {
                    Estimado: 'spline'
                  }
              },
              tooltip: {
                    grouped: false // Default true
              },
              axis: {
                    x: {
                        label: {
                        text: valoresScatterEpsUno[valoresScatterEpsUno.length-1][0].x,
                        position: 'outer-center'
                    },
                      tick: {
                          fit: false
                        }
                    },
                    y: {
                        label: {
                        text: valoresScatterEpsUno[valoresScatterEpsUno.length-1][0].y,
                        position: 'outer-middle'
                      }
                    }
              }
          });
        })
      }
      fn_Scatter_Eps_uno();
        /* ===== TERMINA SECCION GRAFICA SCATTER EPS UNO ===== */


        /* ===== SECCION GRAFICA SCATTER EPS DOS ===== */  
      function fn_Scatter_Eps_dos(){ 
        scatter_eps_dos.getAll().then(function(){

        var valoresScatterEpsDos=scatter_eps_dos.todos;

        var scatter_dos = c3.generate({ 
            bindto: '#scatter_dos',
              padding: {
                  right: 50,
                  left: 50
              },
              size: {
                    height: 300
              },
              color: {
                    pattern: ['#0060B5', '#AF0037']
              },
              data: {
              xs: {
                  Real: 'Real_x',
                  Estimado: 'Estimado_x'
              },
                  // iris data from R
                  columns:valoresScatterEpsDos,
                  type: 'scatter',
                  types: {
                    Estimado: 'spline'
                  }
              },
              tooltip: {
                    grouped: false // Default true
              },
              axis: {
                    x: {
                        label: {
                        text: valoresScatterEpsDos[valoresScatterEpsDos.length-1][0].x,
                        position: 'outer-center'
                    },
                      tick: {
                          fit: false
                        }
                    },
                    y: {
                        label: {
                        text: valoresScatterEpsDos[valoresScatterEpsDos.length-1][0].y,
                        position: 'outer-middle'
                      }
                    }
              }
          });
        })
      }
      fn_Scatter_Eps_dos();
        /* ===== TERMINA SECCION GRAFICA SCATTER EPS DOS ===== */


        /* ===== SECCION GRAFICA SCATTER EPS TRES ===== */  
      function fn_Scatter_Eps_tres(){ 

        scatter_eps_tres.getAll().then(function(){

        var valoresScatterEpsTres=scatter_eps_tres.todos;

        var scatter_tres = c3.generate({ 
            bindto: '#scatter_tres',
              padding: {
                  right: 50,
                  left: 50
              },
              size: {
                    height: 300
              },
              color: {
                    pattern: ['#0060B5', '#AF0037']
              },
              data: {
              xs: {
                  Real: 'Real_x',
                  Estimado: 'Estimado_x'
              },
                  // iris data from R
                  columns:valoresScatterEpsTres,
                  type: 'scatter',
                  types: {
                    Estimado: 'spline'
                  }
              },
              tooltip: {
                    grouped: false // Default true
              },
              axis: {
                    x: {
                        label: {
                        text: valoresScatterEpsTres[valoresScatterEpsTres.length-1][0].x,
                        position: 'outer-center'
                    },
                      tick: {
                          fit: false
                        }
                    },
                    y: {
                        label: {
                        text: valoresScatterEpsTres[valoresScatterEpsTres.length-1][0].y,
                        position: 'outer-middle'
                      }
                    }
              }
          });
        })
      }
      fn_Scatter_Eps_tres();
        /* ===== TERMINA SECCION GRAFICA SCATTER EPS TRES ===== */


        /* ===== SECCION GRAFICA SCATTER IPS UNO ===== */  
      function fn_Scatter_Ips_uno(){ 

        scatter_ips_uno.getAll().then(function(){

        var valoresScatterIpsUno=scatter_ips_uno.todos;

        var scatter_cuatro = c3.generate({ 
            bindto: '#scatter_cuatro',
              padding: {
                  right: 50,
                  left: 50
              },
              size: {
                    height: 300
              },
              color: {
                    pattern: ['#0060B5', '#AF0037']
              },
              data: {
              xs: {
                  Real: 'Real_x',
                  Estimado: 'Estimado_x'
              },
                  // iris data from R
                  columns:valoresScatterIpsUno,
                  type: 'scatter',
                  types: {
                    Estimado: 'spline'
                  }
              },
              tooltip: {
                    grouped: false // Default true
              },
              axis: {
                    x: {
                        label: {
                        text: valoresScatterIpsUno[valoresScatterIpsUno.length-1][0].x,
                        position: 'outer-center'
                    },
                      tick: {
                          fit: false
                        }
                    },
                    y: {
                        label: {
                        text: valoresScatterIpsUno[valoresScatterIpsUno.length-1][0].y,
                        position: 'outer-middle'
                      }
                    }
              }
          });

        })
      }
      fn_Scatter_Ips_uno();
        /* ===== TERMINA SECCION GRAFICA SCATTER IPS UNO ===== */


        /* ===== SECCION GRAFICA SCATTER IPS DOS ===== */  
      function fn_Scatter_Ips_dos(){ 

        scatter_ips_dos.getAll().then(function(){

        var valoresScatterIpsDos=scatter_ips_dos.todos;

        var scatter_cinco = c3.generate({ 
            bindto: '#scatter_cinco',
              padding: {
                  right: 50,
                  left: 50
              },
              size: {
                    height: 300
              },
              color: {
                    pattern: ['#0060B5', '#AF0037']
              },
              data: {
              xs: {
                  Real: 'Real_x',
                  Estimado: 'Estimado_x'
              },
                  // iris data from R
                  columns:valoresScatterIpsDos,
                  type: 'scatter',
                  types: {
                    Estimado: 'spline'
                  }
              },
              tooltip: {
                    grouped: false // Default true
              },
              axis: {
                    x: {
                        label: {
                        text: valoresScatterIpsDos[valoresScatterIpsDos.length-1][0].x,
                        position: 'outer-center'
                    },
                      tick: {
                          fit: false
                        }
                    },
                    y: {
                        label: {
                        text: valoresScatterIpsDos[valoresScatterIpsDos.length-1][0].y,
                        position: 'outer-middle'
                      }
                    }
              }
          });

        })
      }
      fn_Scatter_Ips_dos();

        /* ===== TERMINA SECCION GRAFICA SCATTER IPS DOS ===== */


        /* ===== SECCION GRAFICA SCATTER IPS TRES ===== */  
      function fn_Scatter_Ips_tres(){ 
        scatter_ips_tres.getAll().then(function(){

        var valoresScatterIpsTres=scatter_ips_tres.todos;

        var scatter_seis = c3.generate({ 
            bindto: '#scatter_seis',
              padding: {
                  right: 50,
                  left: 50
              },
              size: {
                    height: 300
              },
              color: {
                    pattern: ['#0060B5', '#AF0037']
              },
              data: {
              xs: {
                  Real: 'Real_x',
                  Estimado: 'Estimado_x'
              },
                  // iris data from R
                  columns:valoresScatterIpsTres,
                  type: 'scatter',
                  types: {
                    Estimado: 'spline'
                  }
              },
              tooltip: {
                    grouped: false // Default true
              },
              axis: {
                    x: {
                        label: {
                        text: valoresScatterIpsTres[valoresScatterIpsTres.length-1][0].x,
                        position: 'outer-center'
                    },
                      tick: {
                          fit: false
                        }
                    },
                    y: {
                        label: {
                        text: valoresScatterIpsTres[valoresScatterIpsTres.length-1][0].y,
                        position: 'outer-middle'
                      }
                    }
              }
          });

        })
      }
      fn_Scatter_Ips_tres();

        /* ===== TERMINA SECCION GRAFICA SCATTER IPS TRES ===== */



        /* ===== SECCION GRAFICA SCATTER SEMANA UNO ===== */  

      function fn_Scatter_Semana_uno(){ 

        scatter_semana_uno.getAll().then(function(){

        var valoresScatterSemanaUno=scatter_semana_uno.todos;

        var scatter_siete = c3.generate({ 
            bindto: '#scatter_siete',
              padding: {
                  right: 50,
                  left: 50
              },
              size: {
                    height: 300
              },
              color: {
                    pattern: ['#0060B5', '#AF0037']
              },
              data: {
              xs: {
                  Real: 'Real_x',
                  Estimado: 'Estimado_x'
              },
                  // iris data from R
                  columns:valoresScatterSemanaUno,
                  type: 'scatter',
                  types: {
                    Estimado: 'spline'
                  }
              },
              tooltip: {
                    grouped: false // Default true
              },
              axis: {
                    x: {
                        label: {
                        text: valoresScatterSemanaUno[valoresScatterSemanaUno.length-1][0].x,
                        position: 'outer-center'
                    },
                      tick: {
                          fit: false
                        }
                    },
                    y: {
                        label: {
                        text: valoresScatterSemanaUno[valoresScatterSemanaUno.length-1][0].y,
                        position: 'outer-middle'
                      }
                    }
              }
          });

        })
      }
      fn_Scatter_Semana_uno();

        /* ===== TERMINA SECCION GRAFICA SCATTER SEMANA UNO ===== */


        /* ===== SECCION GRAFICA SCATTER SEMANA DOS ===== */  
      function fn_Scatter_Semana_dos(){ 

        scatter_semana_dos.getAll().then(function(){

        var valoresScatterSemanaDos=scatter_semana_dos.todos;

        var scatter_ocho = c3.generate({ 
            bindto: '#scatter_ocho',
              padding: {
                  right: 50,
                  left: 50
              },
              size: {
                    height: 300
              },
              color: {
                    pattern: ['#0060B5', '#AF0037']
              },
              data: {
              xs: {
                  Real: 'Real_x',
                  Estimado: 'Estimado_x'
              },
                  // iris data from R
                  columns:valoresScatterSemanaDos,
                  type: 'scatter',
                  types: {
                    Estimado: 'spline'
                  }
              },
              tooltip: {
                    grouped: false // Default true
              },
              axis: {
                    x: {
                        label: {
                        text: valoresScatterSemanaDos[valoresScatterSemanaDos.length-1][0].x,
                        position: 'outer-center'
                    },
                      tick: {
                          fit: false
                        }
                    },
                    y: {
                        label: {
                        text: valoresScatterSemanaDos[valoresScatterSemanaDos.length-1][0].y,
                        position: 'outer-middle'
                      }
                    }
              }
          });

        })
      }
      fn_Scatter_Semana_dos();
        /* ===== TERMINA SECCION GRAFICA SCATTER SEMANA DOS ===== */


        /* ===== SECCION GRAFICA SCATTER SEMANA TRES ===== */  
      function fn_Scatter_Semana_tres(){ 

        scatter_semana_tres.getAll().then(function(){

        var valoresScatterSemanaTres=scatter_semana_tres.todos;

        var scatter_nueve = c3.generate({ 
            bindto: '#scatter_nueve',
              padding: {
                  right: 50,
                  left: 50
              },
              size: {
                    height: 300
              },
              color: {
                    pattern: ['#0060B5', '#AF0037']
              },
              data: {
              xs: {
                  Real: 'Real_x',
                  Estimado: 'Estimado_x'
              },
                  // iris data from R
                  columns:valoresScatterSemanaTres,
                  type: 'scatter',
                  types: {
                    Estimado: 'spline'
                  }
              },
              tooltip: {
                    grouped: false // Default true
              },
              axis: {
                    x: {
                        label: {
                        text: valoresScatterSemanaTres[valoresScatterSemanaTres.length-1][0].x,
                        position: 'outer-center'
                    },
                      tick: {
                          fit: false
                        }
                    },
                    y: {
                        label: {
                        text: valoresScatterSemanaTres[valoresScatterSemanaTres.length-1][0].y,
                        position: 'outer-middle'
                      }
                    }
              }
          });

        })
      }
      fn_Scatter_Semana_tres();
        /* ===== TERMINA SECCION GRAFICA SCATTER SEMANA TRES ===== */
          

        /* == TERMINA GRAFICAS PAGINA LISTAR CALIFICACIONES == */

          var refresh=$interval(function(){
            fn_Scatter_Eps_uno();
            fn_Scatter_Eps_dos();
            fn_Scatter_Eps_tres();
            fn_Scatter_Ips_uno();
            fn_Scatter_Ips_dos();
            fn_Scatter_Ips_tres();
            fn_Scatter_Semana_uno();
            fn_Scatter_Semana_dos();
            fn_Scatter_Semana_tres();
          }, TIME_REFRESH.time);

          $scope.$on('$destroy', function(){
            $interval.cancel(refresh)
          });

          $scope.inicializar();
}]);
