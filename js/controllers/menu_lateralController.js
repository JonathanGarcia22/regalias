'use strict';


//Animacion de contador

function fn_contador_animacion(elemento, texto, tiempo_inicio) {

    var odometer = new Odometer({

        el: $("#" + elemento)[0],

        value: 0,

        theme: 'minimal',

        duration: 1000

    });

    odometer.render();

    $('#' + elemento).text(texto);
}


angular.module('clicSaludApp.menu_lateral', ['clicSaludApp.constantes'])

    .controller('MenuLateralCtrl', ['$scope', '$http', '$window', '$state', '$location', '$stateParams', 'regalias', '$compile',
        function($scope, $http, $window, $state, $location, $stateParams, regalias, $compile) {

            /*  $scope.clickear = function () {
        console.log("click")
      }
*/
            $scope.inicializar = function() {


                $("li.contenido-deslizable > a.menu-item").on('click', function(e) {

                    e.preventDefault();

                    e.stopPropagation();

                    $(".item-contenido-deslizable").removeClass("active");

                    $(this).next(".item-contenido-deslizable").toggleClass("active");

                });



                $("li.contenido-deslizable > a.menu-item").on('mouseenter', function(e) {

                    e.preventDefault();

                    e.stopPropagation();

                    $(".item-contenido-deslizable").removeClass("active");

                });



                $(".fa-chevron-down").click(function() {

                    var $ele = $(this).parents('.panel-heading');

                    $ele.parents(".grid-stack-item-content").toggleClass("por-collapse", 500);

                });



                $(".fa-close").on("click", function() {

                    var $ele = $(this).parents('.grid-stack-item-content');

                    $ele.addClass('panel-close');

                    setTimeout(function() {

                        $ele.parent().remove();

                    }, 0);

                });



                $(".fa-rotate-right").on("click", function() {

                    var $ele = $(this).parents('.panel-heading').siblings('.esconder-caja');

                    $ele.append('<div class="overlay"><div class="overlay-content"><i class="fa fa-refresh fa-2x fa-spin"></i></div></div>');

                    setTimeout(function() {

                        $ele.find('.overlay').remove();

                    }, 2000);



                    /*$('.open').click(function () {
        console.log("click")
       $(this).toggleClass('clicked active');

       $(this).find('span').toggleClass('actived');

      });
*/


                });



                /* ====== GRILLA GRIDSTACK ======== */




                /* == TERMINA GRILLA GRIDSTACK == */



                /* ====== CARRUSEL ====== */



                $('.carousel').carousel({

                    interval: 8000

                })



                /* ====== TERMINA CARRUSEL ====== */


                /* $(document).on("click", ".open", function() {
                 // $('.open').click(function () {
                    $(this).toggleClass('clicked active');

                    $(this).find('span').toggleClass('actived');

                  });*/

                /* $(document).on("click", ".componente", function() {
      // $('.open').click(function () {
        
        $(".open").removeClass("clicked active")


       });
*/

            };



            /* == FUNCION EFECTO FLIP EN CADA CAJA == */
            function rgbToHex(R, G, B) {
                return toHex(R) + toHex(G) + toHex(B)
            }

            function toHex(n) {
                n = parseInt(n, 10);
                if (isNaN(n)) return "00";
                n = Math.max(0, Math.min(n, 255));
                return "0123456789ABCDEF".charAt((n - n % 16) / 16) +
                    "0123456789ABCDEF".charAt(n % 16);
            }



            $scope.flip = function(caja) {

                $("." + caja).toggleClass('flipped');

            };



            /* == TERMINA FUNCION EFECTO FLIP EN CADA CAJA == */



            /* == FUNCION CALENDARIO == */



            (function($) {



                $.fn.bekeyProgressbar = function(options) {



                    options = $.extend({

                        animate: true,

                        animateText: true

                    }, options);



                    var $this = $(this);



                    var $progressBar = $this;

                    var $progressCount = $progressBar.find('.ProgressBar-percentage--count');

                    var $circle = $progressBar.find('.ProgressBar-circle');

                    var percentageProgress = $progressBar.attr('data-progress');

                    var percentageRemaining = (100 - percentageProgress);

                    var percentageText = $progressCount.parent().attr('data-progress');



                    //Calcule la circonférence du cercle

                    var radius = $circle.attr('r');

                    var diameter = radius * 2;

                    var circumference = Math.round(Math.PI * diameter);



                    //Calcule le pourcentage d'avancement

                    var percentage = circumference * percentageRemaining / 100;



                    $circle.css({

                        'stroke-dasharray': circumference,

                        'stroke-dashoffset': percentage

                    })



                    //Animation de la barre de progression

                    if (options.animate === true) {

                        $circle.css({

                            'stroke-dashoffset': circumference

                        }).animate({

                            'stroke-dashoffset': percentage

                        }, 3000)

                    }



                    //Animation du texte (pourcentage)

                    if (options.animateText == true) {

                        $({
                            Counter: 0
                        }).animate(

                            {
                                Counter: percentageText
                            },

                            {
                                duration: 3000,

                                step: function() {

                                    $progressCount.text(Math.ceil(this.Counter) + '%');

                                }

                            });



                    } else {

                        $progressCount.text(percentageText + '%');

                    }



                };



            })(jQuery);



            /* BOTON SANDUCHE*/

            $(document).on("click", ".bttn", function() {

                var display = ($(".contenedor_menu_lateral").css("display"))

                if (display == 'none') {

                    $(".contenedor_menu_lateral").show('fade');

                } else {

                    $(".contenedor_menu_lateral").hide('fade');

                }



            })

            function MaysPrimera(string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            }


            regalias.getAll().then(function() {


                $(document).on("click", ".cerrar", function() {
                    $(".caja").show();
                    $("#expandir").animate({
                        height: "0px",
                        width: "0px"
                    }, 0, function() {
                        $("#expandir").hide();
                    });
                })

                $(document).on("click", ".expandir", function() {

                    var obj = ($(this).attr('class').split(' ')[1]);
                    var cat = (($(this).attr('class').split(' ')[2]))
                    var cat_id = parseInt(cat[cat.length - 1] - 1);
                    var elem = document.querySelector('.' + regalias.todos[obj].categorias[0].color);
                    var style = getComputedStyle(elem);
                    var color = style["background-color"];
                    $("#expandir").css("border", "4px solid " + color);

                    var tabla = "";
                    tabla += "<table id='tabla' class='table table-striped table-responsive tabla_info_regalias'>";
                    if ((regalias.todos[obj].categorias[cat_id].archivos.length == 0)) {
                        var titulos_tabla = [];
                    } else {
                        var titulos_tabla = Object.keys((regalias.todos[obj].categorias[cat_id].archivos[0]));
                    }
                    tabla += "<th class='contenido_tablas celda_min_pequena'>#</th>";

                    $("#titulo_categoria").html('<i class="' + regalias.todos[obj].categorias[cat_id].icono + '"' + 'aria-hidden="true"></i>&nbsp;&nbsp;' + regalias.todos[obj].categorias[cat_id].nombre_categoria)


                    for (var z in titulos_tabla) {
                        var titulo = titulos_tabla[z][0].toUpperCase();
                        var clase = 'celda_mediana';
                        /*if((titulos_tabla[z].search("lace"))!=-1){
                        var clase='celda_pequena';
                        }*/
                        if ((titulos_tabla[z].search("lia")) != -1) {
                            var clase = 'celda_grande';
                        }
                        if (((titulos_tabla[z].search("chi")) != -1) || ((titulos_tabla[z].search("urac")) != -1) || ((titulos_tabla[z].search("sisten")) != -1)) {
                            var clase = 'celda_minv2_pequena';
                        }
                        if ((titulos_tabla[z].search("echa")) != -1) {
                            var clase = 'celda_mediana';
                        }
                        if (((titulos_tabla[z].search("ombre")) != -1) || (titulos_tabla[z].search("escrip")) != -1) {
                            var clase = 'celda_grande';
                        }
                        tabla += "<th class='contenido_tablas" + clase + "'" + ">" + (MaysPrimera(titulos_tabla[z])).replace("_", " ") + "</th>";
                    }



                    //tabla+= "<th class='contenido_tablas celda_mediana'>Entregable</th><th class='contenido_tablas celda_grande'>Nombre Artículo</th><th class='contenido_tablas celda_mediana'>Revista</th><th class='contenido_tablas celda_pequena'>Archivo</th><th class='contenido_tablas celda_grande'>Enlace Internet</th>";
                    var cont = 0;

                    for (var t in regalias.todos[obj].categorias[cat_id].archivos) {
                        // console.log(regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[t]])

                        cont++;

                        tabla += "<tr>";
                        tabla += "<td class='celda_min_pequena'>" + cont + "</td>";
                        for (var y in titulos_tabla) {

                            if (regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]] == "") {

                                tabla += "<td class='celda_min_pequena' style='color:#FF0000;'>" + "Falta" + "</td>";
                            } else {
                                if ((titulos_tabla[y].search("chi")) != -1) { //archivo



                                    // tabla+= "<td class='celda_min_pequena'><a href='"+regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]]+"'>"+"Ver"+"</a></td>";

                                    var archivo = (regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]]).split(",");
                                    if (archivo.length >= 2) {
                                        tabla += "<td class='celda_minv2_pequena'>";
                                        var c = 1;
                                        for (var u = 0; u < archivo.length / 2; u++) {

                                            tabla += "<a href=" + "'" + (archivo[c]) + "'" + " target='_blank'>" + archivo[c - 1] + "</a><br>";
                                            c = c + 2;

                                        }

                                        tabla += "</td>";
                                    } else {
                                        tabla += "<td class='celda_pequena'><a href='" + regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]] + "' target='_blank'>" + "Ver" + "</a></td>";

                                    }




                                } else if ((titulos_tabla[y].search("lace")) != -1) { //enlace
                                    tabla += "<td class='celda_pequena'><a href='" + regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]] + "' target='_blank'>" + "Ver" + "</a></td>";

                                } else if ((titulos_tabla[y].search("lia")) != -1) {
                                    tabla += "<td class='celda_grande'>" + regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]] + "</td>";
                                } else if ((titulos_tabla[y].search("echa")) != -1) {
                                    tabla += "<td class='celda_mediana'>" + regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]] + "</td>";
                                } else if (((titulos_tabla[y].search("ombre")) != -1) || ((titulos_tabla[y].search("escrip")) != -1)) {
                                    tabla += "<td class='celda_grande'>" + regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]] + "</td>";
                                } else if (((titulos_tabla[y].search("urac")) != -1) || ((titulos_tabla[y].search("asist")) != -1)) {
                                    tabla += "<td class='celda_min_pequena'>" + regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]] + "</td>";
                                } else {
                                    tabla += "<td class='celda_mediana'>" + regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]] + "</td>";
                                }

                            }

                        } //else si es null
                        tabla += "</tr>";

                        /*   tabla+= "<td class='celda_mediana'>"+regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[t]]+"</td>";
                           tabla+= "<td class='celda_grande'>"+regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[t]]+"</td>";
                           tabla+= "<td class='celda_mediana'>"+regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[t]]+"</td>";
                           tabla+= "<td class='celda_pequena'><a href='"+regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[t]]+"'>"+"Ver"+"</a></td>";
                           tabla+= "<td class='celda_grande'><a href='"+regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[t]]+"'>"+regalias.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[t]]+"</a></td>";
                          */


                    }

                    tabla += "</table>"
                    if ((regalias.todos[obj].categorias[cat_id].archivos.length != 0)) {

                        $("#tabla").html(tabla);
                    } else {
                        $("#tabla").html("");

                    }
                    //$(".caja").animate({ height: "100%" }, 1000 );
                    /*console.log($(this))
      var clase_agrandar=$(this).attr('class').split(' ');
      $("."+clase_agrandar[1]).css("height", 500);
      $("."+clase_agrandar[1]).css("width", 500);
      $("."+clase_agrandar[1]).css("z-index",999999);
*/

                    /*$("#expandir").css("height", "100%");
      $("#expandir").css("width", "97%");
*/
                    $("#expandir").show();
                    $("#expandir").animate({
                        height: "700px",
                        width: "95.9%"
                    }, 500, function() {
                        //$(".caja").hide();
                    });

                })



                $(document).on("click", ".opcione_menu_lateral", function() {
                    $("#categorias").hide();
                    $(".opcione_menu_lateral").css("color", "#8A8A8A")
                    $(".objetivos").css("color", "#8A8A8A")

                    $(this).css({
                        "color": "#5af"
                        /*,
                              "border": "2px solid !important",
                              "border-bottom-color": "transparent !important", 
                              "border-right-color": "transparent !important", 
                              "border-left-color": "#3E82F5 !important", 
                              "border-top-color": "transparent !important" 
                        */
                    });

                })

                $(document).on("click", ".check_esconder", function() {
                    $("#expandir").hide();
                    var clase = $(this).val();
                    var display = ($("." + clase).css("display"))
                    if (display == 'none') {
                        $("." + clase).show();
                    } else {
                        $("." + clase).hide();
                    }

                    var contador_x = 0;
                    var contador_y = 0;
                    var contador = 0;
                    $('.grid-stack-item.caja').each(function() {
                        if ($(this).css("display") == 'none') {
                        } else {
                            $(this).attr("data-gs-x", contador_x);
                            $(this).attr("data-gs-y", contador_y);
                            contador_x = contador_x + 4;
                            if (contador == 2) {
                                contador_y = contador_y + 3;
                                contador_x = 0;
                                contador = 0;
                            } else {
                                contador++;
                            }
                        }
                    })
                })


                $scope.nombre_objetivos = "";

                var contador = 0;
                $scope.nombre_objetivos += '<ul class="navegacion_lateral">';
                $scope.nombre_objetivos += '<li><a class="componente" ' + ' ng-click=clickear(' + "'" + 'historia' + "'" + ')' + '><span class="opcione_menu_lateral underline-right">Historia</span></a></li>'
                $scope.nombre_objetivos += '<li><a class="componente" ' + ' ng-click=clickear(' + "'" + 'equipo' + "'" + ')' + '><span class="opcione_menu_lateral underline-right">Equipo</span></a></li>'
                $scope.nombre_objetivos += '<li class="open clicked active"><a ' + ' ng-click=clickear(' + "'" + 'home' + "'" + ')' + '><span id="desarrollo_tecnico" class="opcione_menu_lateral underline-right">Desarrollo Técnico</span><span class="arrow"></span></a>'
                $scope.nombre_objetivos += '<nav>'
                $scope.nombre_objetivos += '<ul class="drop">'

                //$scope.nombre_objetivos+=  <li><a name="#medicamentos"><span class="opcione_menu_lateral underline-right"></span>Medicamento</a></li>

                var aCategorias = new Array();
                var aCategorias_nuevaS= new Array();
                aCategorias[0] = regalias.todos["objetivo4"];
                aCategorias[1] = regalias.todos["objetivo2"];
                aCategorias[2] = regalias.todos["objetivo3"];
                aCategorias[3] = regalias.todos["objetivo5"];
                aCategorias[4] = regalias.todos["objetivo6"];
                aCategorias[5] = regalias.todos["objetivo1"];


                for (var i in aCategorias) {


                    contador = contador + 1;
                    if (aCategorias[i].bold == 'Difusión y transferencia tecnológica') {
                        var mostrar_objetivos = 'Difusión y transferencia';
                    } else {
                        var mostrar_objetivos = aCategorias[i].bold;
                    }
                    $scope.nombre_objetivos += '<li><a name="' + "#" + aCategorias[i].orden + '"' + ' class="objetivos "  ' + 'value=' + "'" + aCategorias[i].orden + "'" + 'ng-click=clickear(' + "'" + 'tecnico' + "'" + ')' + '><span class="opcione_menu_lateral underline-right"></span>' + mostrar_objetivos + '</a></li>'
                    // $scope.nombre_objetivos+= '<li><a ui-sref='+"'"+regalias.todos[i].orden+"'"+ ' class="objetivos"  '+ 'value='+ "'"+regalias.todos[i].orden +"'"+ '><span class="opcione_menu_lateral underline-right">'+'Objetivo ' +  contador +'</span></a></li>'   

                }
                $scope.nombre_objetivos += '</ul>';
                $scope.nombre_objetivos += '</nav>';
                $scope.nombre_objetivos += '</li>';
                //$scope.nombre_objetivos+=  '<li><a  class="componente" '+ ' ng-click=clickear('+"'"+'operativo'+ "'"+')'+'><span class="opcione_menu_lateral underline-right">Desarrollo Administrativo</span></a></li>';



                $scope.nombre_objetivos += '<li class="open clicked active nueva_seccion"><a ' + ' ng-click=clickear(' + "'" + 'home' + "'" + ')' + '><span id="nueva_seccion" class="opcione_menu_lateral underline-right">Nueva_sección</span><span class="arrow"></span></a>'
                $scope.nombre_objetivos += '<nav>'
                $scope.nombre_objetivos += '<ul class="drop">'

                // ACA EMPIEZA EL NUEVO DEPLEGABLE PARA LA NUEVA SECCION SECCION
                $scope.nombre_objetivos += '<li class="nueva_seccion"><a name="' + "#" + regalias.todos["objetivo7"].orden + '"' + ' class="objetivos "  ' + 'value=' + "'" + regalias.todos["objetivo7"].orden + "'" + 'ng-click=clickear(' + "'" + 'tecnico' + "'" + ')' + '><span class="opcione_menu_lateral underline-right"></span>' + "Etapa 1" + '</a></li>'
                $scope.nombre_objetivos += '<li class="nueva_seccion"><a name="' + "#" + regalias.todos["objetivo8"].orden + '"' + ' class="objetivos "  ' + 'value=' + "'" + regalias.todos["objetivo8"].orden + "'" + 'ng-click=clickear(' + "'" + 'tecnico' + "'" + ')' + '><span class="opcione_menu_lateral underline-right"></span>' + "Etapa 2" + '</a></li>'
                $scope.nombre_objetivos += '<li class="nueva_seccion"><a name="' + "#" + regalias.todos["objetivo9"].orden + '"' + ' class="objetivos "  ' + 'value=' + "'" + regalias.todos["objetivo9"].orden + "'" + 'ng-click=clickear(' + "'" + 'tecnico' + "'" + ')' + '><span class="opcione_menu_lateral underline-right"></span>' + "Etapa 3" + '</a></li>'
                $scope.nombre_objetivos += '<li class="nueva_seccion"><a name="' + "#" + regalias.todos["objetivo10"].orden + '"' + ' class="objetivos "  ' + 'value=' + "'" + regalias.todos["objetivo10"].orden + "'" + 'ng-click=clickear(' + "'" + 'tecnico' + "'" + ')' + '><span class="opcione_menu_lateral underline-right"></span>' + "Etapa 4" + '</a></li>'


                $scope.nombre_objetivos += '</ul>';
                $scope.nombre_objetivos += '</nav>';
                $scope.nombre_objetivos += '<li><a class="componente" ' + ' ng-click=clickear(' + "'" + 'consolidado' + "'" + ')' + '><span class="opcione_menu_lateral underline-right">Consolidado</span></a></li>'
                $scope.nombre_objetivos += '<li><a  class="componente" ' + ' ng-click=clickear(' + "'" + 'prorroga' + "'" + ')' + '><span class="opcione_menu_lateral underline-right">Prorroga</span></a></li>';
                $scope.nombre_objetivos += '</ul>';
                /*console.log(Object.keys(regalias.todos).length)
                for(var i=6; i<7; i++) {
                  console.log("entra", regalias.todos[i]);
                  //aCategorias_nuevaS.push(regalias.todos[i]);
                  console.log(aCategorias_nuevaS)
                  //$scope.nombre_objetivos += '<li><a name="' + "#" + aCategorias[i].orden + '"' + ' class="objetivos "  ' + 'value=' + "'" + aCategorias[i].orden + "'" + 'ng-click=clickear(' + "'" + 'tecnico' + "'" + ')' + '><span class="opcione_menu_lateral underline-right"></span>' + mostrar_objetivos + '</a></li>'
                }*/

                $compile($("#nombre_objetivos").append($scope.nombre_objetivos))($scope); //mostrar contenido dinamico

                //al hacer click en la nueva sección, se desapareceran los textos que tienen resalte azul
                $(document).on("click", ".nueva_seccion", function() {
                   $("#nueva_seccion").css("color", "#5af");
                   $("#desarrollo_tecnico").css("color", "#8A8A8A"); //se vuelve a gris el desarrollo tecnico

                   
                })

                $(document).on("click", ".objetivos", function() {
                    $(".opcione_menu_lateral").each(function() {
                        if ($(this).text() == "Desarrollo Técnico") {
                            $(this).css("color", "#5af")
                        } else {
                            $(this).css("color", "#8A8A8A")
                        }
                    })

                    $(".objetivos").css("color", "#8A8A8A")
                    //$(".opcione_menu_lateral").eq(6).css( "color","red !important");
                    $(this).css({
                        "color": "#5af"
                        /*,
                              "border": "2px solid !important",
                              "border-bottom-color": "transparent !important", 
                              "border-right-color": "transparent !important", 
                              "border-left-color": "#3E82F5 !important", 
                              "border-top-color": "transparent !important" 
                        */
                    });

                    $(this).toggleClass('activa_items');
                    $("#expandir").hide();
                    var valor_objetivo = ($(this).attr("value"));
                    $scope.objetivoclick = valor_objetivo;
                    $("#objetivoclick").val($scope.objetivoclick)
                    $("#categorias").hide(200);
                    fn_categorias(valor_objetivo)
                    fn_crear_tarjetas(valor_objetivo);
                })



                function fn_categorias(objetivo) {
                    if ($scope.include.search("dash") != -1) {
                        $("#categorias").show(200);
                        var categorias = "";
                        for (var i in regalias.todos[objetivo].categorias) {
                            categorias += "<div class='check-container'>";
                            categorias += "<input type='checkbox' name='categoria[]' checked value=" + "'" + regalias.todos[objetivo].categorias[i].id_categoria + "'" + " id=" + "'" + regalias.todos[objetivo].categorias[i].nombre_categoria + "'" + " class='check_esconder'></input>";
                            categorias += "<label for=" + "'" + regalias.todos[objetivo].categorias[i].nombre_categoria + "'" + "></label>";
                            categorias += " <span class='tag'>" + regalias.todos[objetivo].categorias[i].nombre_categoria + "</span>";
                            categorias += "</div>";
                        }
                        $("#categorias").html(categorias);
                    }
                }
                fn_categorias($stateParams.objetivo == '' ? 'objetivo2' : $stateParams.objetivo)
                //fn_crear_tarjetas($stateParams.objetivo==''?'objetivo2':$stateParams.objetivo);




                function fn_crear_tarjetas(objetivo) {



                    var tarjetas = "";



                    //tarjetas+='<div  class="grid-stack grid-stack-animate" data-gs-width="12" data-gs-animate="yes">';

                    var contador_x = 0;
                    var contador_y = 0;
                    var contador = 0;
                    var contador_t = 0;
                    var ultimo_Car = regalias.todos[objetivo].orden[regalias.todos[objetivo].orden.length - 1];
                    //$(".objetivo_id").text("Objetivo "+ ultimo_Car+": ");
                    $(".objetivo_id").text("");

                    $(".txt-medio.descripcion_objetivo").text(regalias.todos[objetivo].nombre);
                    $(".bold_objetivo").html("" + regalias.todos[objetivo].bold + "");
                    for (var i in regalias.todos[objetivo].categorias) {


                        tarjetas += '<div id="contenedor-flip" class="caja ' + objetivo + " " + regalias.todos[objetivo].categorias[i].id_categoria + ' grid-stack-item"' + ' objetivo="' + objetivo + '"' + ' data-gs-x=' + contador_x + ' data-gs-y=' + contador_y + ' data-gs-width="4" data-gs-height="3" data-gs-min-width="4" data-gs-min-height="3" data-gs-no-resize="true" data-gs-no-move="true">';

                        tarjetas += '<div class="grid-stack-item-content caraFlipUno">';

                        tarjetas += '<div class="' + regalias.todos[objetivo].categorias[i].color + '"' + '>';

                        tarjetas += '<div class="row">';

                        tarjetas += '<div class="col-xs-12 col-sm-12">';

                        tarjetas += '<div class="panel-heading" id="panel-opciones-desplegables">';

                        tarjetas += ' <h3 class="panel-title">';

                        //<!--NOMBRE CAJA-->

                        tarjetas += '<span></span><span class="titulos_cartas">' + '<i class="' + regalias.todos[objetivo].categorias[i].icono + '"' + 'aria-hidden="true"></i>' + "&nbsp;&nbsp;" + regalias.todos[objetivo].categorias[i].nombre_categoria + '</span>';

                        //<!--TERMINA NOMBRE CAJA-->

                        tarjetas += ' <ul class="panel-acciones-desplegables opciones-usuario">'

                        //tarjetas+=' <li><i class="expandir '+  objetivo+" "+ regalias.todos[objetivo].categorias[i].id_categoria+ ' fa fa-arrows-alt"></i></li>';
                        tarjetas += ' <li><span class="expandir ' + objetivo + " " + regalias.todos[objetivo].categorias[i].id_categoria + ' btn_abrir_tabla_info"><i class="fa fa-arrows-alt"></i></span></li>';

                        tarjetas += '</ul>';

                        tarjetas += ' </h3>'

                        tarjetas += '</div>';

                        tarjetas += '</div>';

                        tarjetas += '</div>';

                        tarjetas += '<div class="esconder-caja">';

                        tarjetas += '<div class="row">'

                        tarjetas += '<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">';

                        tarjetas += '<div class="circulos_tarjetas_blanco">';

                        tarjetas += '<div class="informacion_grafica">';

                        tarjetas += '<div id="acategoria' + contador_t + '"' + ' class="contador animated fadeIn"></div>'

                        tarjetas += '</div>';

                        tarjetas += '</div>';

                        tarjetas += '</div>';

                        tarjetas += '</div>';

                        tarjetas += '</div>';

                        tarjetas += '</div>';

                        tarjetas += '</div>';

                        tarjetas += '<div class="grid-stack-item-content caraFlipDos carta_medicamento_blanca_reversa">';

                        tarjetas += '<div class="row">';

                        tarjetas += '<div class="col-xs-12 col-sm-12">';

                        tarjetas += '<div class="panel-heading" id="panel-opciones-desplegables">';

                        tarjetas += '<h3 class="panel-title">';

                        tarjetas += '<span></span><span class="titulos_cartas_azul">Uso APP</span>';



                        tarjetas += '</h3>';

                        tarjetas += '</div>';

                        tarjetas += '</div>';

                        tarjetas += '</div>';

                        tarjetas += '<div class="esconder-caja">';

                        //<!--AQUI VA UNA GRAFICA-->

                        tarjetas += '<div class="row">';

                        tarjetas += '<div class="col-xs-12 col-sm-12 col-md-12 cont_reve">';

                        tarjetas += '<div class="banda_cartas_blanca_reverso">';



                        tarjetas += '</div>';

                        tarjetas += '<div class="boton_carta_reverso">'

                        tarjetas += '<a><p><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></p></a>';

                        tarjetas += '</div>';

                        tarjetas += '</div>';

                        tarjetas += '</div>';



                        tarjetas += '</div>';

                        tarjetas += '</div>';

                        tarjetas += '</div>';

                        contador_x = contador_x + 4;


                        if (contador == 2) {
                            contador_y = contador_y + 3;
                            contador_x = 0;
                            contador = 0;

                        } else {
                            contador++;

                        }




                        contador_t++;



                        $("#linea_contenedor_azul").removeClass();
                        $("#linea_contenedor_azul").addClass(regalias.todos[objetivo].categorias[i].color + "_linea");


                    }




                    $(".tarjetas").html(tarjetas);

                    contador_t = 0;
                    for (var i in regalias.todos[objetivo].categorias) {
                        var odo = "acategoria" + contador_t;
                        fn_contador_animacion(odo, regalias.todos[objetivo].categorias[i].archivos.length)
                        contador_t++;
                    }


                    //tarjetas+= '</div>';



                }



            })// fin then


            /* == GRAFICAS PAGINA DASHBOARD == */

            var formatNumber = d3.format(",.0f"); //Formato miles




            /* == TERMINA GRAFICAS PAGINA DASHBOARD == */


            //FILTRO DE CATEGORIAS

            $('input[name="categoria[]"]').click(function() {

                $('input[name="categoria[]"]').each(function() {

                    if (!$(this).prop("checked")) {

                        $("#seccion_" + $(this).val()).hide();

                    } else {

                        $("#seccion_" + $(this).val()).show("show");

                    }

                });



            })


            $scope.inicializar();
            /*
                $(document).on("click",  function() {
                  // $('.open').click(function () {
                         $('.grid-stack').gridstack({

                       width: 12,

                       cellHeight: 60,

                       verticalMargin: 10,

                       animate :true

                   });

             

                   });
            */


        }
    ]);