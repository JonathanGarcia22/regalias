﻿'use strict';

angular.module('clicSaludApp.home', ['clicSaludApp.constantes'])

.controller('HomeCtrl', ['$scope', '$http', '$window', '$state', '$location', '$stateParams',
function ($scope, $http, $window, $state, $location, $stateParams) {
    $scope.autenticarUsuario = function () {
        $state.go('dashboard');
    }
}]);
