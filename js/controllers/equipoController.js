

'use strict';



 

    

   //Animacion de contador

  function fn_contador_animacion(elemento,texto,tiempo_inicio){ 

   var odometer = new Odometer({

      el: $("#"+elemento)[0],

      value: 0,

      theme: 'minimal',

      duration: 1000

    });

    odometer.render();

    $('#'+elemento).text(texto);
  }

 

angular.module('clicSaludApp.equipo', ['clicSaludApp.constantes'])


.controller('equipoCtrl', ['$scope', '$http', '$window', '$state', '$location', '$stateParams','TIME_REFRESH','regalias_equipo','$compile',

function ($scope, $http, $window, $state, $location, $stateParams,TIME_REFRESH,regalias_equipo,$compile) {


    $('.grid-stack').gridstack({

               width: 12,

               cellHeight: 60,

               verticalMargin: 10,

               animate :true

           });




      /* == FUNCION EFECTO FLIP EN CADA CAJA == */
  function rgbToHex(R,G,B) {return toHex(R)+toHex(G)+toHex(B)}
    function toHex(n) {
     n = parseInt(n,10);
     if (isNaN(n)) return "00";
     n = Math.max(0,Math.min(n,255));
     return "0123456789ABCDEF".charAt((n-n%16)/16)
          + "0123456789ABCDEF".charAt(n%16);
  }

 

      $scope.flip=function(caja) {

               $("."+caja).toggleClass('flipped');

      };

 

      /* == TERMINA FUNCION EFECTO FLIP EN CADA CAJA == */

 

      /* == FUNCION CALENDARIO == */

 


    /* BOTON SANDUCHE*/

   $(document).on("click", ".bttn", function() {

      var display= ($(".contenedor_menu_lateral").css("display"))

      if(display=='none'){

        $(".contenedor_menu_lateral").show('fade');

      }

      else{

        $(".contenedor_menu_lateral").hide('fade');

      }

 

    })

   function MaysPrimera(string){
   return string.charAt(0).toUpperCase() + string.slice(1);
    }






       regalias_equipo.getAll().then(function(){


      $(document).on("click", ".cerrar", function() {
      $(".caja").show();
      $("#expandir2").animate({ height: "0px",width: "0px" }, 0,function() {
        $("#expandir2").hide();
      });
    })

   $(document).on("click", ".expandir2", function() {
    var obj=($(this).attr('class').split(' ')[1]);
    var cat=(($(this).attr('class').split(' ')[2]))
    var cat_id=parseInt(cat[cat.length-1]-1);
    var elem = document.querySelector('.'+regalias_equipo.todos[obj].categorias[0].color);
    var style = getComputedStyle(elem);
    var color=style["background-color"];
    $("#expandir2").css("border", "4px solid "+color);

    var tabla="";
    tabla+="<table id='tabla' class='table table-striped table-responsive tabla_info_regalias'>";
    if((regalias_equipo.todos[obj].categorias[cat_id].archivos.length==0) ){
      var titulos_tabla=[];
    }
    else{
    var titulos_tabla=Object.keys((regalias_equipo.todos[obj].categorias[cat_id].archivos[0]));
  }
    tabla+="<th class='contenido_tablas celda_min_pequena'>#</th>";
    
    $("#titulo_categoria").html('<i class="'+regalias_equipo.todos[obj].categorias[cat_id].icono+'"' +'aria-hidden="true"></i>&nbsp;&nbsp;'+regalias_equipo.todos[obj].categorias[cat_id].nombre_categoria)
    

    for(var z in titulos_tabla){
      var titulo=titulos_tabla[z][0].toUpperCase();
      var clase='celda_mediana';
      /*if((titulos_tabla[z].search("lace"))!=-1){
      var clase='celda_pequena';
      }*/
      if((titulos_tabla[z].search("lia"))!=-1) { 
        var clase='celda_grande';
      }
      if( ((titulos_tabla[z].search("chi"))!=-1) || ((titulos_tabla[z].search("urac"))!=-1)  || ((titulos_tabla[z].search("sisten"))!=-1)) { 
        var clase='celda_minv2_pequena';
      }
      if((titulos_tabla[z].search("echa"))!=-1) { 
        var clase='celda_mediana';
      }
      if(((titulos_tabla[z].search("ombre"))!=-1) || (titulos_tabla[z].search("escrip"))!=-1  ) { 
        var clase='celda_grande';
      }
      tabla+="<th class='contenido_tablas"+  clase+"'"+">"+(MaysPrimera(titulos_tabla[z])).replace("_"," ")+"</th>";
    }

    

    //tabla+= "<th class='contenido_tablas celda_mediana'>Entregable</th><th class='contenido_tablas celda_grande'>Nombre Artículo</th><th class='contenido_tablas celda_mediana'>Revista</th><th class='contenido_tablas celda_pequena'>Archivo</th><th class='contenido_tablas celda_grande'>Enlace Internet</th>";
    var cont=0;

      for( var t in regalias_equipo.todos[obj].categorias[cat_id].archivos){
       // console.log(regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[t]])

              cont++;

              tabla+= "<tr>";
              tabla+= "<td class='celda_min_pequena'>"+cont+"</td>";
              for (var y in titulos_tabla){

                if(regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]]==""){

                   tabla+= "<td class='celda_min_pequena' style='color:#FF0000;'>"+"Falta"+"</td>";
                }

                else{
                  if((titulos_tabla[y].search("chi"))!=-1){ //archivo

           

                              // tabla+= "<td class='celda_min_pequena'><a href='"+regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]]+"'>"+"Ver"+"</a></td>";

                              var archivo=(regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]]).split(",");
                    
                              if(archivo.length>=2){
                                tabla+= "<td class='celda_minv2_pequena'>";
                                var c=1;
                                for(var u=0; u<archivo.length/2; u++){

                                     tabla+="<a href="+"'"+(archivo[c])+"'"+" target='_blank'>"+archivo[c-1]+"</a><br>";
                                     c=c+2;

                                }

                                tabla+="</td>";
                            }
                            else{
                              tabla+= "<td class='celda_pequena'><a href='"+regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]]+"' target='_blank'>"+"Ver"+"</a></td>";

                            }
             

             

           

                  }
                  else if((titulos_tabla[y].search("lace"))!=-1){ //enlace
                    tabla+= "<td class='celda_pequena'><a href='"+regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]]+"' target='_blank'>"+"Ver"+"</a></td>";

                  }

                 else if((titulos_tabla[y].search("lia"))!=-1){
                     tabla+= "<td class='celda_grande'>"+regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]]+"</td>";
                  }
                  else if((titulos_tabla[y].search("echa"))!=-1){
                     tabla+= "<td class='celda_mediana'>"+regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]]+"</td>";
                  }
                  else if(((titulos_tabla[y].search("ombre"))!=-1) || ((titulos_tabla[y].search("escrip"))!=-1) ){
                     tabla+= "<td class='celda_grande'>"+regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]]+"</td>";
                  }
                  else if( ((titulos_tabla[y].search("urac"))!=-1) || ((titulos_tabla[y].search("asist"))!=-1) ) {
                     tabla+= "<td class='celda_min_pequena'>"+regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]]+"</td>";
                  }

                  else{ 
                     tabla+= "<td class='celda_mediana'>"+regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[y]]+"</td>";
                  }

              }

            }//else si es null
              tabla+="</tr>";         

           /*   tabla+= "<td class='celda_mediana'>"+regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[t]]+"</td>";
              tabla+= "<td class='celda_grande'>"+regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[t]]+"</td>";
              tabla+= "<td class='celda_mediana'>"+regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[t]]+"</td>";
              tabla+= "<td class='celda_pequena'><a href='"+regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[t]]+"'>"+"Ver"+"</a></td>";
              tabla+= "<td class='celda_grande'><a href='"+regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[t]]+"'>"+regalias_equipo.todos[obj].categorias[cat_id].archivos[t][titulos_tabla[t]]+"</a></td>";
             */
             
  
      }

   tabla+="</table>"
    if((regalias_equipo.todos[obj].categorias[cat_id].archivos.length!=0) ){

     $("#tabla").html(tabla);
    }
    else{
     $("#tabla").html("");

    }
      //$(".caja").animate({ height: "100%" }, 1000 );
      /*console.log($(this))
      var clase_agrandar=$(this).attr('class').split(' ');
      $("."+clase_agrandar[1]).css("height", 500);
      $("."+clase_agrandar[1]).css("width", 500);
      $("."+clase_agrandar[1]).css("z-index",999999);
*/
    
     /*$("#expandir2").css("height", "100%");
      $("#expandir2").css("width", "97%");
*/
     $("#expandir2").show();
     $("#expandir2").animate({ height: "700px",width: "95.9%" }, 500,function() {
          //$(".caja").hide();
      } );

    })
 
 

    function fn_categorias(objetivo){


        $("#categorias").show(200);

 

        var categorias="";
        //$stateParams.objetivo=objetivo
          console.log(objetivo)

        for(var i in regalias_equipo.todos[objetivo].categorias){
          categorias+="<div class='check-container'>";

          categorias+= "<input type='checkbox' name='categoria[]' checked value="+"'"+regalias_equipo.todos[objetivo].categorias[i].id_categoria+"'"+ " id="+"'"+regalias_equipo.todos[objetivo].categorias[i].nombre_categoria+"'"+" class='check_esconder'></input>";

          categorias+="<label for="+"'"+regalias_equipo.todos[objetivo].categorias[i].nombre_categoria+"'"+"></label>";

          categorias+=" <span class='tag'>"+regalias_equipo.todos[objetivo].categorias[i].nombre_categoria+"</span>";

          categorias+= "</div>";     

        }


        $("#categorias").html(categorias);


     }
         //fn_categorias($stateParams.objetivo==''?'objetivo2':$stateParams.objetivo)
      fn_crear_tarjetas($stateParams.objetivo==''?'objetivo2':$stateParams.objetivo);

  

 

    function fn_crear_tarjetas(objetivo){

 

  var tarjetas="";



//tarjetas+='<div  class="grid-stack grid-stack-animate" data-gs-width="12" data-gs-animate="yes">';

var contador_x=0;
var contador_y=0;
var contador=0;
var contador_t=0;
var ultimo_Car=regalias_equipo.todos[objetivo].orden[regalias_equipo.todos[objetivo].orden.length-1];
$(".objetivo_id").text("Objetivo "+ ultimo_Car+": ");
$(".objetivo_id").text("");

$(".txt-medio.descripcion_objetivo").text(regalias_equipo.todos[objetivo].nombre);
$(".bold_objetivo").html(""+regalias_equipo.todos[objetivo].bold+"");
for (var i in regalias_equipo.todos[objetivo].categorias){


tarjetas+= '<div id="contenedor-flip" class="caja '+ objetivo+" "+regalias_equipo.todos[objetivo].categorias[i].id_categoria+' grid-stack-item"'+ ' objetivo="'+objetivo+'"'+' data-gs-x=' + contador_x+ ' data-gs-y='+ contador_y+ ' data-gs-width="4" data-gs-height="3" data-gs-min-width="4" data-gs-min-height="3" data-gs-no-resize="true" data-gs-no-move="true">';

tarjetas+=               '<div class="grid-stack-item-content caraFlipUno">';

tarjetas+=                     '<div class="'+regalias_equipo.todos[objetivo].categorias[i].color +'"'+'>';

tarjetas+=              '<div class="row">';

tarjetas+=               '<div class="col-xs-12 col-sm-12">';

tarjetas+=          '<div class="panel-heading" id="panel-opciones-desplegables">';

tarjetas+= ' <h3 class="panel-title">';

//<!--NOMBRE CAJA-->

tarjetas+='<span></span><span class="titulos_cartas">'+'<i class="'+regalias_equipo.todos[objetivo].categorias[i].icono +'"' +'aria-hidden="true"></i>'+"&nbsp;&nbsp;" +regalias_equipo.todos[objetivo].categorias[i].nombre_categoria+'</span>';

//<!--TERMINA NOMBRE CAJA-->

tarjetas+=' <ul class="panel-acciones-desplegables opciones-usuario">'

//tarjetas+=' <li><i class="expandir2 '+  objetivo+" "+ regalias_equipo.todos[objetivo].categorias[i].id_categoria+ ' fa fa-arrows-alt"></i></li>';
//tarjetas+=' <li><span class="expandir2 '+  objetivo+" "+ regalias_equipo.todos[objetivo].categorias[i].id_categoria+ ' btn_abrir_tabla_info"><i class="fa fa-arrows-alt"></i></span></li>';

tarjetas+='</ul>';

tarjetas+=' </h3>'

tarjetas+=  '</div>';

tarjetas+='</div>';

tarjetas+='</div>';

tarjetas+='<div class="esconder-caja">';

tarjetas+= '<div class="row">'

tarjetas+= '<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">';

tarjetas+='<div class="circulos_tarjetas_blanco">';

tarjetas+=  '<div class="informacion_grafica">';

tarjetas+=           '<div id="acategoria'+contador_t+'"'+   ' class="contador animated fadeIn"></div>'

tarjetas+=  '</div>';

tarjetas+='</div>';

tarjetas+='</div>';

tarjetas+= '</div>';

tarjetas+= '</div>';

tarjetas+='</div>';

tarjetas+=   '</div>';

tarjetas+='<div class="grid-stack-item-content caraFlipDos carta_medicamento_blanca_reversa">';

tarjetas+='<div class="row">';

tarjetas+= '<div class="col-xs-12 col-sm-12">';

tarjetas+='<div class="panel-heading" id="panel-opciones-desplegables">';

tarjetas+='<h3 class="panel-title">';

tarjetas+='<span></span><span class="titulos_cartas_azul">Uso APP</span>';



tarjetas+= '</h3>';

tarjetas+= '</div>';

tarjetas+='</div>';

tarjetas+='</div>';

tarjetas+='<div class="esconder-caja">';

//<!--AQUI VA UNA GRAFICA-->

tarjetas+='<div class="row">';

tarjetas+= '<div class="col-xs-12 col-sm-12 col-md-12 cont_reve">';

tarjetas+='<div class="banda_cartas_blanca_reverso">';



tarjetas+='</div>';

tarjetas+='<div class="boton_carta_reverso">'

 tarjetas+= '<a><p><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></p></a>';

tarjetas+='</div>';

tarjetas+= '</div>';

tarjetas+='</div>';



tarjetas+='</div>';

tarjetas+='</div>';

tarjetas+= '</div>';

contador_x=contador_x+4;


if(contador==2){
  contador_y=contador_y+3;
  contador_x=0;
  contador=0;

}
else{
  contador++;

}




contador_t++;



$("#linea_contenedor_equipo").removeClass();
$("#linea_contenedor_equipo").addClass(regalias_equipo.todos[objetivo].categorias[i].color+"_linea");


}




$(".tarjetas").html(tarjetas);
contador_t=0;
for (var i in regalias_equipo.todos[objetivo].categorias){
  var odo="acategoria"+contador_t;
  fn_contador_animacion(odo,regalias_equipo.todos[objetivo].categorias[i].archivos.length)
  contador_t++;

}


//tarjetas+= '</div>';

 

     }



   })



}]);