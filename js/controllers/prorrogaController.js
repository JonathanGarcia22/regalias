'use strict';

angular.module('clicSaludApp.prorroga', ['clicSaludApp.constantes'])

.controller('ProrrogaCtrl', ['$scope', '$http', '$window', '$state', '$location', '$stateParams',
function ($scope, $http, $window, $state, $location, $stateParams) {
    $scope.inicializar = function () {
      $(".toggle-btn").on('click', function() {
      $(".logo-container").toggleClass("nav-min");
      $(".menu-lateral-portal").toggleClass("nav-min");
      $(".body-wrapper").toggleClass("nav-min");
      setTimeout(function() {
        //initializeCharts();
        }, 800);
      });

    $("li.contenido-deslizable > a.menu-item").on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();
      $(".item-contenido-deslizable").removeClass("active");
      $(this).next(".item-contenido-deslizable").toggleClass("active");
    });

    $("li.contenido-deslizable > a.menu-item").on('mouseenter', function(e) {
      e.preventDefault();
      e.stopPropagation();
      $(".item-contenido-deslizable").removeClass("active");
    });

    $('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function(){
      $(this).toggleClass('open');
    });

    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true
    });
    
  };
    $scope.inicializar();
}]);
